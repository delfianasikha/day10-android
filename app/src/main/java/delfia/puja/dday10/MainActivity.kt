package delfia.puja.dday10

import android.app.Activity
import android.content.Intent
import android.icu.text.SimpleDateFormat
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap

class MainActivity : AppCompatActivity(), View.OnClickListener {

    var daftarmhs = mutableListOf<HashMap<String,String>>()
    var daftarprodi = mutableListOf<String>()
    var imStr = ""
    var pilihprodi =""
    lateinit var AdapterDataMhs : AdapterDataMhs
    lateinit var adapProd : ArrayAdapter<String>
    lateinit var  adapMhs : SimpleAdapter
    lateinit var MediaHelper : MediaHelper
    var uri = "http://192.168.43.58/Kampus/show_data.php"
    var uri1 = "http://192.168.43.58/Kampus/show_prodi.php"
    var  uri3 ="http://192.168.43.58/Kampus/query_data.php"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AdapterDataMhs =  AdapterDataMhs(daftarmhs,this)
        LsMhs.layoutManager = LinearLayoutManager(this)
        LsMhs.adapter = AdapterDataMhs
        //LsMhs.addOnItemTouchListener(itemTouch)
        adapProd = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,daftarprodi)
        SpProdi.adapter = adapProd
        SpProdi.onItemSelectedListener = itemSelect

        MediaHelper = MediaHelper(this)
        imageView2.setOnClickListener(this)
        btInsert.setOnClickListener(this)
        btDel.setOnClickListener(this)
        btUpdate.setOnClickListener(this)
        btFind.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        getProdi()
        ShowMhs()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if (requestCode == MediaHelper.getRcGalery()){
                imStr = MediaHelper.getBitmapToString(data!!.data,imageView2)
            }
        }
    }

    //nampil prodi
    fun getProdi(){
        val request  = StringRequest(Request.Method.POST,uri1,
            Response.Listener { response ->
                daftarprodi.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarprodi.add(jsonObject.getString("nama_prodi"))
                }
                adapProd.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->  })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
    val itemSelect = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            SpProdi.setSelection(0)
            pilihprodi = daftarprodi.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihprodi = daftarprodi.get(position)
        }

    }

    /**
    val itemTouch = object  : RecyclerView.OnItemTouchListener{
    override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {
    }
    override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
    val view = rv.findChildViewUnder(rv.x,e.y)
    val tag = rv.getChildAdapterPosition(view!!)
    val pos = daftarprodi.indexOf(daftarmhs.get(tag).get("nama_prodi"))

    SpProdi.setSelection(pos)
    txNm.setText(daftarmhs.get(tag).get("nim").toString())
    txNamaMhs.setText(daftarmhs.get(tag).get("nama").toString())
    Picasso.get().load(daftarmhs.get(tag).get("url")).into(imageView2)
    return false
    }

    override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
    }

    }
     **/
    fun ShowMhs(){
        val request = StringRequest(Request.Method.POST,uri,Response.Listener { response ->
            daftarmhs.clear()
            val jsonArray = JSONArray(response)
            for (x in 0..(jsonArray.length()-1)){
                val jsonObject = jsonArray.getJSONObject(x)
                var mhs = HashMap<String,String>()
                mhs.put("nim",jsonObject.getString("nim"))
                mhs.put("nama",jsonObject.getString("nama"))
                mhs.put("nama_prodi",jsonObject.getString("nama_prodi"))
                mhs.put("url",jsonObject.getString("url"))
                //mhs.put("tanggal",jsonObject.getString("tangal_lahir"))
                //mhs.put("jenis_kelamin",jsonObject.getString("jenis_kelamin"))
                // mhs.put("alamat",jsonObject.getString("alamat"))
                daftarmhs.add(mhs)
            }
            AdapterDataMhs.notifyDataSetChanged()

        },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Kesalahan Saat Konfigurasi",Toast.LENGTH_SHORT).show()
            })

        val queue =  Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imageView2->{
                val intent  = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent,MediaHelper.getRcGalery())
            }
            R.id.btInsert->{
                query("insert")
                ShowMhs()

            }
            R.id.btDel->{
                query("delete")
                ShowMhs()
            }
            R.id.btUpdate->{
                query("update")
                ShowMhs()
            }
            R.id.btFind->{

            }

        }
    }

    //query

    fun query(mode : String){
        val request = object : StringRequest(Method.POST,uri3,
            Response.Listener { response ->
                val jsonobject  = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil ", Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(this,"Operasi Gagal ", Toast.LENGTH_LONG).show()
                } },
            Response.ErrorListener { error ->
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                var nmFile = "DC"+SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())+".jpg"
                when(mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("nim",txNm.text.toString())
                        hm.put("nama",txNamaMhs.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("nama_prodi",pilihprodi)
                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("nim",txNm.text.toString())
                        hm.put("nama",txNamaMhs.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("nama_prodi",pilihprodi)
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("nim",txNm.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

}