package delfia.puja.dday10

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*


class AdapterDataMhs(val dataMhs: List<HashMap<String,String>>, val mainActivity : MainActivity): RecyclerView.Adapter<AdapterDataMhs.HolderDataMhs> (){
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterDataMhs.HolderDataMhs {
        val v  =LayoutInflater.from(parent.context).inflate(R.layout.row_mhs,parent,false)
        return HolderDataMhs(v)
    }

    override fun getItemCount(): Int {
        return dataMhs.size
    }

    override fun onBindViewHolder(holder: AdapterDataMhs.HolderDataMhs, position: Int) {
        val data =dataMhs.get(position)
        holder.txnim.setText(data.get("nim"))
        holder.txnama.setText(data.get("nama"))
        holder.txProd.setText(data.get("nama_prodi"))
        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(holder.photo)
        //holder.tgl.setText(data.get("tanggal"))
        // holder.jenis_kel.setText(data.get("jenis_kelamin"))
        // holder.alamat.setText(data.get("alamat"))
        if (position.rem(2) == 0) holder.layouts.setBackgroundColor(Color.rgb(230,245,240))
        else holder.layouts.setBackgroundColor(Color.rgb(255,255,245))

        holder.layouts.setOnClickListener(View.OnClickListener{
            val pos = mainActivity.daftarprodi.indexOf(data.get("nama_prodi"))
            mainActivity.SpProdi.setSelection(pos)
            mainActivity.txNm.setText(data.get("nim"))
            mainActivity.txNamaMhs.setText(data.get("nama"))
            Picasso.get().load(data.get("url")).into(mainActivity.imageView2)
            /**
            txNm.setText(daftarmhs.get(tag).get("nim").toString())
            txNamaMhs.setText(daftarmhs.get(tag).get("nama").toString())
            Picasso.get().load(daftarmhs.get(tag).get("url")).into(imageView2)
             */
        })
    }

    class HolderDataMhs(v : View) :  RecyclerView.ViewHolder(v){
        val txnim = v.findViewById<TextView>(R.id.nim)
        val txnama = v.findViewById<TextView>(R.id.namaMhs)
        val txProd = v.findViewById<TextView>(R.id.Prodi)
        val photo = v.findViewById<ImageView>(R.id.ImgUpload)
        val layouts = v.findViewById<ConstraintLayout>(R.id.rowlay)
        //val tgl =v.findViewById<TextView>(R.id.txTgl)
        //val jenis_kel  = v.findViewById<TextView>(R.id.txJenis_kel)
        //val alamat = v.findViewById<TextView>(R.id.txAlamat)
    }
}